module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps : [

    // First application
    {
      name      : 'express',
      script    : './bin/www',
      // env: {
      //   COMMON_VARIABLE: 'true',
      //     PORT: 3000
      // },
      env_production : {
        NODE_ENV: 'production'
      }
    }
  ],

  /**
   * Deployment section
   * http://pm2.keymetrics.io/docs/usage/deployment/
   */
  deploy : {
    production : {
      user : 'root',
      host : '107.172.143.192',
      ref  : 'origin/master',
      repo : 'https://gitlab.com/s2118231/express-template.git',
      path : '/root/express/production',
      'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production'
    },
    // dev : {
    //   user : 'node',
    //   host : '212.83.163.1',
    //   ref  : 'origin/master',
    //   repo : 'https://gitlab.com/s2118231/express-template.git',
    //   path : '/var/www/development',
    //   'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env dev',
    //   env  : {
    //     NODE_ENV: 'dev'
    //   }
    // }
  }
};
